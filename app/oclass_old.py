from appJar import gui
import postgresql
import re, json
from calendar import monthrange
import datetime as dt
from pprint import pprint

months=['январь', 'февраль','март', 'апрель', 'май', 'июнь','июль', 'август', 'сентябрь','октябрь','ноябрь', 'декабрь']
years=range(2000, 2100)


with open('/home/ovi/Learn_Project/vtc_application/app/data.json') as indata:
  data = json.load(indata)
pprint(data)
ur = {
  'name': 'none',
  'id': 'none',
  'login': data['login']
}
dbparam = {'user': 'ovi',
           'pas': '1953',
           'db': 'vtk',
           'schem': 'one'}
try:
  db = postgresql.open('pq://{0}:{1}@localhost:5432/{2}'.format(dbparam['user'], dbparam['pas'], dbparam['db']))
except TypeError as identifier:
  pass
else:
  pass

def login(log, pas):
  us = db.query("""SELECT u.id, u.name FROM {2}.users u 
  WHERE u.login='{0}' AND u.pas='{1}'""".format(log, pas, dbparam['schem']))
  if len(us) == 1:
    ur['id'] = us[0][0]
    ur['name'] = us[0][1]
    ur['login'] = log
    ur['pas'] = pas
    with open('data.json', 'w') as out:
      json.dump("'login':'{}'".format(ur['login']), out)
    return True
  else:
    return False

def list_cols(table, withid=False):
  """Return columns tabel without 'id'
      name, type, size  
  Arguments:
    table {string} -- name table
    withid {boolean} -- output with id or without id
  """
  tb = db.query("""SELECT column_name, data_type, character_maximum_length 
    FROM information_schema.columns
    WHERE table_schema='{0}' AND table_name='{1}'""".format(dbparam['schem'], table))
  if len(tb)>1:
    cols={}
    i=0
    for x in tb:
      if not withid and (x[0]=='id' or x[0]==str(table)+'_id'):
        continue
      n=x[0]
      s=x[2]
      if re.search(r'timestamp', str(x[1])):
        t='tmstamp'
      elif re.search(r'character varying', x[1]):
        t='varchar'
      else:
        t=x[1]
      cols[i]={
        'name':n,
        'type':t,
        'size':s
      }
      i+=1
    return cols
  else:
    return None

def find_key(table, cols):
  """Вывод внешних клчей для таблицы
    в массив с ее столбцами
  Arguments:
    table {name} -- Название таблицы
    cols {list} -- List column (name, type, size)
  """
  fk = db.query("""SELECT
      kcu.column_name,
      ccu.table_name AS foreign_table_name,
      ccu.column_name AS foreign_column_name
    FROM
      information_schema.table_constraints AS tc
      JOIN information_schema.key_column_usage AS kcu
        ON tc.constraint_name = kcu.constraint_name
      JOIN information_schema.constraint_column_usage AS ccu
        ON ccu.constraint_name = tc.constraint_name
    WHERE constraint_type = 'FOREIGN KEY' AND tc.table_schema='{0}' AND tc.table_name='{1}';""".format(dbparam['schem'], table))
  print(str(fk))
  print(str(len(cols)))
  if len(cols)>0 and len(fk)>0:
    for x in cols:
      print(str(x))
      for y in range(0, len(fk)):
        if cols[x]['name']==fk[y][0]:
          cols[x]['fk_table']=fk[y][1]
          cols[x]['fk_col']=fk[y][2]
    return cols
  else: return None




class Win(object):
  def __init__(self, ms):
    self.m = ms
    self.create_data_pick()
    self.connect()
  def connect(self):
    self.m.startSubWindow('Login', 'Login', True)
    self.m.addImage('pers', 'img/pers.png', 0, 1, 1)
    self.m.addLabel('lblogmes', 'Для продолжения войдите в систему', 1, 0, 3)
    self.m.addLabel('lblog', 'Login', 3, 0)
    self.m.addEntry('enlog', 3, 1, 2) 
    self.m.addLabel('lbpas', 'Password', 4, 0)
    self.m.addSecretEntry('enpas', 4, 1, 2)
    self.m.addButton('Cancel', exit, 6, 0)
    self.m.addButton('Submit', self.log, 6, 2)
    self.m.stopSubWindow()
  def exit(self):
    self.m.stop()
  def log(self, event):
    l =self.m.getEntry('enlog')
    p =self.m.getEntry('enpas')
    if len(l)<4:
      self.m.setLabel('lblogmes', 'Логин не соответствует\n требуемой длине')
    #TODO: изменить на 6 
    elif len(p)<4:
      self.m.setLabel('lblogmes', 'Пароль не соответствует\n требуемой длине')
    else:
      stat = login(l, p)
      if stat==True:
        self.m.hideSubWindow('Login')
        self.m.show()
      elif stat==False:
        self.m.errorBox('Ошибка!', 'Ошибка входа в систему!\nПроверьте введеные данные')
  def generate(self, table, wname):
    tb = list_cols(table, True)
    tb = find_key(table, tb)
    self.m.startSubWindow('table', wname, True)
    now = dt.datetime.now()
    for x in tb:
      print(str(tb[x]))
      t=tb[x]['type']
      self.m.addLabel('l'+str(x), str(tb[x]['name']), x, 0)
      if not 'fk_table' in tb[x]:
        print(str(t))
        if t in ['varchar','smallint','integer']:
          self.m.addEntry('inp'+str(x), x, 1, 2)
        elif t in ['tmstamp', 'date']:
          self.m.addLabel('imp'+str(x), '{3}:{4} {0}.{1}.{2}'.format(now.day, now.month, now.year, now.hour, now.minute), x, 1, 1)
          self.m.setLabelSubmitFunction('imp'+str(x), self.open_data_pick)
        else:
          pass 
    self.m.stopSubWindow()
    self.m.hide()
    self.m.showSubWindow('table')
  def create_data_pick(self):
    self.m.startSubWindow('datapicker', 'Редактирование даны и времени')
    self.m.label('ldyear', 'Год', 0, 0, 2)
    self.m.addSpinBox('dyear', ['2000', '2001'], 1, 0, 2)
    self.m.label('ldmonth', 'Месяц', 2, 0, 1)
    self.m.addListBox('dmonth', months, 3, 0, 1)
    self.m.setListBoxFunction('dmonth', self.set_days)
    self.m.label('ldday', 'День', 2, 1)
    self.m.addListBox('dday', ['Выберите', 'год и месяц'], 3, 1, 1)
    self.m.selectListItem('dmonth', 1)
    self.m.addButton("CLOSE", self.m.hideSubWindow, 4, 0)
    self.m.addNamedButton( 'dsubmit', "SUBMIT", self.m.submit, 4, 1)
    self.m.stopSubWindow()
  def set_days(self, ev):
    year=self.m.getSpinBox('dyear')
    month=self.m.getListBox('dmonth')
    
    count_month = monthrange(2018, 2)[1]
    self.m.updateListBox('dday', range(1, count_month), select=False)
    print('{}:{}:{}'.format(str(year), month, count_month))
  def open_data_pick(self, ev):
    self.dateto = ev
    self.m.showSubWindow('datapicker')

    
   def start(self, tb):
        self.listSelect = []
        if tb == 'orders':
            self.tb.setColumnCount(6)

            #Взаимосвязаны 2 строки
            self.listSelect = db.listorder()
            cblist = iter(self.colsRU)
            next(cblist)
            self.cbcol.clear()
            self.cbcol.addItems(cblist)
            self.setRows()
            self.rup.toggled.connect(self.sort)
            self.rlow.toggled.connect(self.sort)

    def setRows(self):
        self.tb.setHorizontalHeaderLabels(self.colsRU)
        ls = iter(self.listSelect)
        next(ls)
        self.tb.setColumnHidden(0, True)
        self.tb.setRowCount(0)
        i = 0
        for x in ls:
            self.tb.setRowCount(i+1)
            tm = QtWidgets.QTableWidgetItem(str(x[0]))
            self.tb.setItem(i, 0, tm)
            tm = QtWidgets.QTableWidgetItem(str(x[4]))
            self.tb.setItem(i, 1, tm)
            tm = QtWidgets.QTableWidgetItem(str(x[1].strftime("%Y-%m-%d %H:%M:%S")))
            self.tb.setItem(i, 2, tm)
            tm = QtWidgets.QTableWidgetItem(str(x[2].strftime("%Y-%m-%d")))
            self.tb.setItem(i, 3, tm)
            tm = QtWidgets.QTableWidgetItem(str(x[5]))
            self.tb.setItem(i, 4, tm)
            tm = QtWidgets.QTableWidgetItem(str(x[6]))
            self.tb.setItem(i, 5, tm)
            i +=1
            print(str(x))
        self.tb.itemSelectionChanged.connect(self.slRow)

    def sort(self):
        if self.rup.isChecked():
            self.listSelect = db.listorder('ORDER BY {} ASC'.format(self.colsDB[by+1]))
            self.setRows()
        elif self.rlow.isChecked():
            self.listSelect = db.listorder('ORDER BY {} DESC'.format(self.colsDB[by+1]))
            self.setRows()

    def slRow(self):
        self.curRow = self.tb.currentRow()
        self.bdel.setEnabled(True)
        self.bedit.setEnabled(True)

        self.bdel.clicked.connect(self.delRow)
        self.bedit.clicked.connect(self.editRow)

        self.lbtn.setText('Выделена: {}'.format(self.curRow + 1))
        print(str(self.curRow))

    def delRow(self):
        db.delel()
    def editRow(self):
        pass
