import postgresql
import json
import operator
from PyQt5.QtCore import QAbstractTableModel, QVariant, Qt

with open('/home/ovi/Learn_Project/vtc_application/app/data.json') as indata:
    data = json.load(indata)
print(str(data))

usr = {'id': None, 'name': None, 'log': data['login'], 'pwd': None}

defDb = {'user': 'ovi', 'pas': '1953', 'db': 'vtk', 'schem': 'one'}


# Main class for work in database
class Odb():
    def __init__(self, param=defDb):
        super().__init__()
        self.con = 'pq://{}:{}@{}:5432/{}'.format(param['user'], param['pas'],
                                                  'localhost', param['db'])
        self.schem = str(param['schem'])

    def login(self, log, pwd):
        u = False
        with postgresql.open(self.con) as d:
            u = d.query(
                "SELECT u.id, u.name FROM {}.users u WHERE u.login=\'{}\' AND u.pas=\'{}\'".
                format(self.schem, log, pwd))
            print(str(u))
            if u:
                usr['id'] = u[0][0]
                usr['name'] = u[0][1]
                usr['log'] = log
                usr['pwd'] = pwd
                return True
            else:
                return False
    
    def opencon(self):
        self.connect = postgresql.open(self.con)

    def addication(self, table, cols, ret='RETURNING id'):
        """Inserting new element
        Arguments:
            table {str} -- table
            cols {list} -- list cols
            ret {str} -- params
        Returns:
            str -- id
        """
        columns = ''
        values = ''
        for x in cols:
            columns += (str(x) + ', ')
            values += "'{}', ".format(cols[x])
        columns = columns[:-2]
        values = values[:-2]
        print(str(columns) + ' ' + str(values))
        with postgresql.open(self.con) as d:
            try:
                return d.query(
                    '''INSERT INTO {}.{}({}) VALUES ({}) {};'''.format(
                        self.schem, table, columns, values, ret))
            except postgresql.exceptions.NotNullError:
                return 'Не допустимое пустое поле'
            except postgresql.exceptions.UniqueError:
                return 'Данные клиент уже существует'

    def selectall(self, table, cols, params='', with_id=True):
        columns = ''
        print(str(type(cols)))

        if str(type(cols)) != "<class 'str'>":
            res = [[]]
            res[0].append(table)
            if with_id == True:
                columns += 'id, '
            for x in cols:
                columns += (str(x) + ', ')
                res[0].append(str(x))
            print(str(res))
            columns = columns[:-2]
            with postgresql.open(self.con) as d:
                sel = d.query('''SELECT {2} FROM {0}.{1} {3}'''.format(
                    self.schem, table, columns, params))
                for x in sel:
                    xi = []
                    for i in x:
                        xi.append(i)
                    res.append(xi)
            return res
        else:
            print('ERROR: cols not LIST!')
            return False

    def selectorder(self, params=''):

        res = [[]]
        res[0].append('oders')
        with postgresql.open(self.con) as d:
            sel = d.query('''SELECT
                o.id, o.date_set as dset, o.deadline as ddl, o.add_cost, c.name as cust, st.name as st,
                (SELECT SUM(eq.cost) FROM one.order_to_equipment ote JOIN one.equipment eq ON eq.id = ote.equipment_id WHERE o.id = order_id) as sumeq
            FROM one.orders o
            JOIN one.customers c
                ON c.id = o.customer_id
            JOIN one.status st
                ON st.id = o.status_id
            {}'''.format(params))
        for x in sel:
            xi = []
            for i in x:
                xi.append(i)
            res.append(xi)
        return res

    def delel(self, tb, param, col='id', ad=''):
        with postgresql.open(self.con) as d:
            return d.execute('DELETE FROM {}.{} WHERE {}={} {}'.format(
                self.schem, tb, col, param, ad))

    def update(self, tb, col, value, param='id=0'):
        with postgresql.open(self.con) as d:
            return d.execute('UPDATE {}.{} SET {}={} WHERE {}'.format(
                self.schem, tb, col, value, param))


db = Odb(defDb)


class tbModel(QAbstractTableModel):
    def __init__(self, datain, headerdata, parent=None, *args):
        QAbstractTableModel.__init__(self, parent, *args)
        self.datain = datain
        self.header = headerdata
        self.p = parent


    def rowCount(self, index):
        return len(self.datain)

    def columnCount(self, index):
        return len(self.header)

    def headerData(self, col, orentation, role):
        if orentation == Qt.Horizontal and role == Qt.DisplayRole:
            return str(self.header[col])
        return QVariant()

    def data(self, index, role):
        if role == Qt.DisplayRole:
            return  str(self.datain[index.row()][index.column()])
        return QVariant()

    def sort(self, col, order):
        self.layoutAboutToBeChanged.emit()
        if order==1:
            self.datain = sorted(self.datain, key=operator.itemgetter(col))
        elif order==0:
            self.datain = sorted(self.datain, key=operator.itemgetter(col), reverse=True)
        self.layoutChanged.emit()


# my function

def fromSecond(ls):
    ls = iter(ls)
    next(ls)
    ls = list(ls)
    return ls


def ifError(el, s=True):
    if s == True:
        el.setStyleSheet('color: red')
    else:
        el.setStyleSheet('color: green')


def fFilter(list, el, t='All', exp=['empty']):
    #Рализация регистро независимого поиска
    el.clear()
    if list:
        list = iter(list)
        next(list)
        if t == 'All' or t == '':
            for x in list:
                if x[0] not in exp:
                    el.addItem((x[1]))
        else:
            for x in list:
                if x[0] not in exp and x[1].lower().find(t.lower()) != -1:
                    el.addItem(str(x[1]))
    else:
        print(
            'Error in fFilter, input data: \nlist: {} \nel:{} \ntext:{} \nexp: {}'.
            format(list, el, t, exp))


def setCurTime(chb, tmb):
    if chb.isChecked():
        tmb.setDateTime(datetime.datetime.now())
        tmb.setEnabled(False)
    else:
        tmb.setEnabled(True)


def getId(t, ls, col=1):
    for x in ls:
        if t == x[col]:
            return x[0]


def getText(id, ls, col=1):
    for x in ls:
        if id == x[0]:
            return x[col]
