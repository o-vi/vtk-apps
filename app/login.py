# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'gui/login.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Flogin(object):
    def setupUi(self, Flogin):
        Flogin.setObjectName("Flogin")
        Flogin.setWindowModality(QtCore.Qt.WindowModal)
        Flogin.resize(280, 320)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(Flogin.sizePolicy().hasHeightForWidth())
        Flogin.setSizePolicy(sizePolicy)
        Flogin.setContextMenuPolicy(QtCore.Qt.NoContextMenu)
        Flogin.setWindowTitle("Login")
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("../icon/ic_person_black_24px.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        Flogin.setWindowIcon(icon)
        Flogin.setStyleSheet("background-color: rgb(255, 237, 237)")
        Flogin.setLocale(QtCore.QLocale(QtCore.QLocale.Russian, QtCore.QLocale.Russia))
        Flogin.setModal(True)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(Flogin)
        self.verticalLayout_2.setContentsMargins(8, 3, 8, 3)
        self.verticalLayout_2.setSpacing(15)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.verticalLayout_3 = QtWidgets.QVBoxLayout()
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.label_3 = QtWidgets.QLabel(Flogin)
        self.label_3.setStyleSheet("image: url(:/icon/ic_person_black_24px.svg);\n"
"height: 30px;")
        self.label_3.setObjectName("label_3")
        self.verticalLayout_3.addWidget(self.label_3)
        self.lhead = QtWidgets.QLabel(Flogin)
        self.lhead.setObjectName("lhead")
        self.verticalLayout_3.addWidget(self.lhead)
        self.verticalLayout_2.addLayout(self.verticalLayout_3)
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setContentsMargins(0, -1, -1, -1)
        self.verticalLayout.setObjectName("verticalLayout")
        self.label = QtWidgets.QLabel(Flogin)
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setItalic(False)
        self.label.setFont(font)
        self.label.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.label.setLocale(QtCore.QLocale(QtCore.QLocale.Russian, QtCore.QLocale.Russia))
        self.label.setAlignment(QtCore.Qt.AlignBottom|QtCore.Qt.AlignHCenter)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.llog = QtWidgets.QLineEdit(Flogin)
        self.llog.setStyleSheet("")
        self.llog.setObjectName("llog")
        self.verticalLayout.addWidget(self.llog)
        self.label_2 = QtWidgets.QLabel(Flogin)
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setItalic(False)
        self.label_2.setFont(font)
        self.label_2.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.label_2.setLocale(QtCore.QLocale(QtCore.QLocale.Russian, QtCore.QLocale.Russia))
        self.label_2.setAlignment(QtCore.Qt.AlignBottom|QtCore.Qt.AlignHCenter)
        self.label_2.setObjectName("label_2")
        self.verticalLayout.addWidget(self.label_2)
        self.lpwd = QtWidgets.QLineEdit(Flogin)
        self.lpwd.setStyleSheet("")
        self.lpwd.setText("")
        self.lpwd.setEchoMode(QtWidgets.QLineEdit.Password)
        self.lpwd.setObjectName("lpwd")
        self.verticalLayout.addWidget(self.lpwd)
        self.verticalLayout_2.addLayout(self.verticalLayout)
        self.bdialog = QtWidgets.QDialogButtonBox(Flogin)
        self.bdialog.setStyleSheet("border-style: solid;\n"
"border-color: rgba(62, 123, 255, 200);\n"
"border-width: 2px;\n"
"padding: 3px 20px;\n"
"border-radius: 5px;")
        self.bdialog.setOrientation(QtCore.Qt.Horizontal)
        self.bdialog.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.bdialog.setCenterButtons(True)
        self.bdialog.setObjectName("bdialog")
        self.verticalLayout_2.addWidget(self.bdialog)

        self.retranslateUi(Flogin)
        self.bdialog.accepted.connect(Flogin.accept)
        self.bdialog.rejected.connect(Flogin.reject)
        QtCore.QMetaObject.connectSlotsByName(Flogin)

    def retranslateUi(self, Flogin):
        _translate = QtCore.QCoreApplication.translate
        self.label_3.setText(_translate("Flogin", "<html><head/><body><p align=\"center\"><br/></p></body></html>"))
        self.lhead.setText(_translate("Flogin", "<html><head/><body><p align=\"center\"><span style=\" font-size:18pt; font-weight:600;\">Вход в систему</span></p></body></html>"))
        self.label.setText(_translate("Flogin", "Логин"))
        self.label_2.setText(_translate("Flogin", "Пароль"))
