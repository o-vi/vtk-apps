# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'gui/view.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_fmView(object):
    def setupUi(self, fmView):
        fmView.setObjectName("fmView")
        fmView.resize(794, 520)
        self.gridLayout = QtWidgets.QGridLayout(fmView)
        self.gridLayout.setObjectName("gridLayout")
        self.tb = QtWidgets.QTableView(fmView)
        self.tb.setFrameShape(QtWidgets.QFrame.Box)
        self.tb.setFrameShadow(QtWidgets.QFrame.Raised)
        self.tb.setLineWidth(1)
        self.tb.setMidLineWidth(2)
        self.tb.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.tb.setObjectName("tb")
        self.tb.horizontalHeader().setDefaultSectionSize(150)
        self.tb.horizontalHeader().setMinimumSectionSize(120)
        self.tb.horizontalHeader().setStretchLastSection(True)
        self.gridLayout.addWidget(self.tb, 0, 0, 6, 5)
        self.bedit = QtWidgets.QPushButton(fmView)
        self.bedit.setEnabled(False)
        self.bedit.setObjectName("bedit")
        self.gridLayout.addWidget(self.bedit, 6, 4, 1, 1)
        self.bdel = QtWidgets.QPushButton(fmView)
        self.bdel.setEnabled(False)
        self.bdel.setObjectName("bdel")
        self.gridLayout.addWidget(self.bdel, 6, 3, 1, 1)
        self.lbtn = QtWidgets.QLabel(fmView)
        self.lbtn.setObjectName("lbtn")
        self.gridLayout.addWidget(self.lbtn, 6, 2, 1, 1)

        self.retranslateUi(fmView)
        QtCore.QMetaObject.connectSlotsByName(fmView)

    def retranslateUi(self, fmView):
        _translate = QtCore.QCoreApplication.translate
        fmView.setWindowTitle(_translate("fmView", "GroupBox"))
        fmView.setTitle(_translate("fmView", "Просмотр"))
        self.bedit.setText(_translate("fmView", "Редактир"))
        self.bdel.setText(_translate("fmView", "Удалить"))
        self.lbtn.setText(_translate("fmView", "<html><head/><body><p align=\"center\">Выделите строку</p></body></html>"))

