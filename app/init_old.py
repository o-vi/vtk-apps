#!/usr/bin/python3.5
# system module
import sys, os, json, random
# module for db
import postgresql
# module for gui
from tkinter import ttk
from appJar import gui
# my classes
import oclass
from oclass import Win


#connect to database
# dbparam = {'user': 'ovi',
#            'pas': '1953',
#            'db': 'vtk',
#            'shem': 'one'}
# db = postgresql.open('pq://{0}:{1}@localhost:5432/{2}'.format(dbparam['user'], dbparam['pas'], dbparam['db']))


app = gui('Admin DB', useTtk=True)
app.setTtkTheme('default')
print(str(app.getTtkTheme()))
app.setResizable(canResize=False)
app.setSize('200x300')
app.setLocation('CENTER')
app.setSticky("news")
app.setExpand("both")
# app.hideTitleBar()
win = Win(app)
 
win.generate('orders','Пользователи')

app.go()



#
#Old class
#
#Main class for work in database
class Odb():
  def __init__(self, db):
    super().__init__()
    self.main = db


#global variable for use DataBase from other classes
db = Odb(opendb)


#Class for login dialog
class Olog(Ui_Flogin):
  def __init__(self, dial, w):
    Ui_Flogin.__init__(self)
    self.setupUi(dial)
    self.dial = dial
    self.main = w
    self.bdialog.accepted.connect(self.accept)
    self.bdialog.rejected.connect(self.reject)

  def accept(self):
    log = self.llog.text()
    pwd = self.lpwd.text()
    if len(log) < 6:
        self.lhead.setText('Не допустимая длина логина')
        print('Логин')
    elif len(log) < 5:
        self.lhead.setText('Недопустимая длина пароля')
        print('Пароль')
    else:
        self.lhead.setText('Все ОК!')
        print('All Ok')
        self.main.show()
        self.dial.hide()

  def reject(self):
    appStop()

#main class


class Oapp(Ui_MainWindow):
  def __init__(self, win):
    Ui_MainWindow.__init__(self)
    self.setupUi(win)
    self.w = win

#
#Old init
#


#!/usr/bin/python3.5
import sys
import os
import random
import postgresql
from PyQt5.QtWidgets import QApplication, QDialog, QMainWindow
from oclass import Oapp, Olog, Odb
from oclass import Odb


global appStop
global loginOk


def loginOk():
  if w.isHidden:
    w.show


def appStop():
  app.stop()


global app

app = QApplication(sys.argv)
dialog = QDialog()
w = QMainWindow()


log = Olog(dialog, w)
dialog.show()


main = Oapp(w)

sys.exit(app.exec_())
