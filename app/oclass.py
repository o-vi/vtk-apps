#!/usr/bin/python3.5
import sys
import os
import random
import datetime
from PyQt5 import QtWidgets, QtCore
from login import Ui_Flogin
from main import Ui_MainWindow as Fmain
from mMenu import Ui_mainMenu

import frames
from classdb import *



# Class for login dialog
class Olog(QtWidgets.QDialog, Ui_Flogin):
    def __init__(self, parent):
        super(Olog, self).__init__(parent)
        self.setupUi(self)
        self.parent = parent
        if usr['log'] != None:
            self.llog.setText(usr['log'])
        self.show()

    def accept(self):
        log = self.llog.text()
        pwd = self.lpwd.text()
        if len(log) < 4:
            self.lhead.setText('Проверьте логин!')
        elif len(pwd) < 4:
            self.lhead.setText('Проверьте пароль!')
        else:
            self.lhead.setText('Запрос пользователя...')
        l = db.login(log, pwd)
        if l == True:
            self.parent.start()
            self.close()
        elif l == False:
            self.lhead.setText('Неверный логин и пароль!')

# Main Class
class Oapp(QtWidgets.QMainWindow, Fmain):
    def __init__(self, parent=None):
        super(Oapp, self).__init__(parent)
        Olog(self)

    def start(self):
        self.setupUi(self)
        self.setWindowTitle(str(usr['name']))
        self.BoxMenu = mMenu(self.centralwidget, self)
        self.BoxCur = self.BoxMenu
        self.gridLayout.addWidget(self.BoxCur, 1, 0, 1, 5)
        self.tomenu.clicked.connect(lambda: self.fchange(mMenu))
        self.show()

    def addcust(self):
        # addication customer
        self.fchange(eCust)

    def editcust(self, id):
        # change data for customer
        self.fchange(eCust)
        self.toback.setEnabled(True)
        self.BoxCur.editor(id)

    def viewcust(self):
        # view list castomers
        self.fchange(fView)
        self.BoxCur.start('customers')

    def addequip(self):
        # Добавление оборудования
        self.fchange(eEqui)

    def editequip(self, id):
        # change data for equipment
        self.fchange(eEqui)
        self.toback.setEnabled(True)
        self.BoxCur.editor(id)

    def vieweq(self):
        # view list equipment
        self.fchange(fView)
        self.BoxCur.start('equipment')

    def addorder(self):
        # addication order
        self.fchange(eOrder)

    def editorder(self, id):
        # change order data
        self.fchange(eOrder)
        self.toback.setEnabled(True)
        self.BoxCur.editor(id)

    def vieworder(self):
        # view list orders
        self.fchange(fView)
        self.BoxCur.start('orders')


    def fchange(self, new, tb=None):
        self.BoxOld = self.BoxCur
        self.toback.clicked.connect(self.fback)
        self.BoxOld.hide()
        self.BoxCur = None
        self.CurClass = new
        self.BoxCur = new(self.centralwidget, self)
        self.gridLayout.addWidget(self.BoxCur, 1, 0, 1, 5)
        self.BoxCur.show()
    def fback(self):
        self.BoxCur.hide()
        self.BoxCur = self.BoxOld
        self.BoxCur.show()
    def freset(self):
        self.BoxCur.hide()
        del self.BoxCur
        self.BoxCur = self.CurClass(self.centralwidget, self)
        self.gridLayout.addWidget(self.BoxCur, 1, 0, 1, 5)
        self.BoxCur.show()

#main Menu
class mMenu(QtWidgets.QGroupBox, Ui_mainMenu):
    def __init__(self, parent, obj):
        super(mMenu, self).__init__(parent)
        self.setupUi(self)
        self.p = obj
        self.show()
        self.boxParams.hide()
        self.boxParams.setTitle('None')
        self.lbSearch.hide()
        self.lbEdit.hide()
        self.lbAdd.clicked.connect(self.add)
        self.lbView.clicked.connect(self.view)
        self.bPart.hide()
    def add(self):
        self.boxParams.setTitle('Создать/Добавить')
        self.bCust.show()
        self.bOrder.show()
        self.bEquip.show()
        self.bCustListorders.hide()
        self.bCustOrder.hide()

        self.bCust.clicked.connect(self.p.addcust)
        self.bEquip.clicked.connect(self.p.addequip)
        self.bOrder.clicked.connect(self.p.addorder)
        self.boxParams.show()
    def view(self):
        self.boxParams.setTitle('Просмотреть')

        self.bCust.clicked.connect(self.p.viewcust)
        self.bEquip.clicked.connect(self.p.vieweq)
        self.bOrder.clicked.connect(self.p.vieworder)

        self.bCust.show()
        self.bEquip.show()
        self.bOrder.show()
        self.bPart.hide()
        self.bCustListorders.hide()
        self.bCustOrder.hide()
        self.boxParams.show()

#addication customer
class eCust(QtWidgets.QGroupBox, frames.Ui_newCust):
    def __init__(self, parent, win):
        super(eCust, self).__init__(parent)
        self.setupUi(self)
        self.show
        self.errorh.hide()
        self.errort.hide()
        self.addinputs = {}
        self.ai = 2
        self.bbox.button(QtWidgets.QDialogButtonBox.Save).setText('Сохранить')
        self.bbox.button(QtWidgets.QDialogButtonBox.Open).setText('Изменить')
        self.bbox.button(QtWidgets.QDialogButtonBox.Reset).setText('Сбросить')
        self.formGroupBox_2.hide()
        #bind
        #self.baddInput.clicked.connect(self.addinput)
        self.cdata = {'id': None}
        self.bbox.button(QtWidgets.QDialogButtonBox.Save).clicked.connect(
            self.save)
        self.bbox.button(QtWidgets.QDialogButtonBox.Reset).clicked.connect(
            win.freset)

    def save(self):
        e = False
        up = False
        dt = {

        }

        if self.cdata['id']:
            up = True
        if self.ename.text():
            dt['name'] = str(self.ename.text())
            self.lname.setStyleSheet('color: #333333;')
        else:
            self.lname.setStyleSheet('color: red;')
            e = True
        if self.edirector.text():
            dt['director'] = str(self.edirector.text())
            self.ldirector.setStyleSheet('color: #333333;')
        else:
            self.ldirector.setStyleSheet('color: red;')
            e = True
        if self.eaccounter.text():
            dt['accountant'] = str(self.eaccounter.text())
            self.laccounter.setStyleSheet('color: #333333;')
        else:
            self.laccounter.setStyleSheet('color: red;')
            e = True
        if self.eemail.text(): dt['contact_email'] = str(self.eemail.text())
        if self.ephone.text():
            dt['contact_phone'] = str(self.ephone.text())
            self.lphone.setStyleSheet('color: #333333;')
        else:
            self.lphone.setStyleSheet('color: red;')
            e = True
        ifError(self.errorh, True)
        ifError(self.errort, True)
        id = 0
        if e == False:
            if up == True:
                for k, v in dt.items():
                    if self.cdata[k] != v:
                        db.update('customers', str(k), '\'{}\''.format(v), 'id={}'.format(self.cdata['id']))
                        id += 1
                if id > 0:
                    self.errorh.setText('Обновлено')
                    ifError(self.errorh, False)
                    self.errort.setText('плей: {}'.format(id))
                    ifError(self.errort, False)
            else:
                id = db.addication('customers', dt)[0][0]
                if not id:
                    self.errorh.setText('Ошибка!')
                else:
                    self.errorh.setText('Успешно!')
                    self.errort.setText('Данные успешно сохранены')
                    ifError(self.errorh, False)
                    ifError(self.errort, False)
                    self.editor(id)
        else:
            self.errorh.setText('Ошибка')
            self.errort.setText('Заплните пустые поля')
        self.errorh.show()
        self.errort.show()

    def editor(self, id):
        self.cdata = {'id':id}
        select = db.selectall('customers',
            ['name', 'director','accountant', 'contact_phone','contact_email'],
            'WHERE id={}'.format(id))
        for i in range(0, len(select[0])):
            self.cdata[select[0][i]] = select[1][i]

        self.ename.setText(str(self.cdata['name']))
        self.edirector.setText(str(self.cdata['director']))
        self.eaccounter.setText(str(self.cdata['accountant']))
        self.ephone.setText(str(self.cdata['contact_phone']))
        self.eemail.setText(str(self.cdata['contact_email']))


    def addinput(self):
        if self.ai <= 5:
            self.addHn = QtWidgets.QLineEdit(self.formGroupBox_2)
            self.addTn = QtWidgets.QLineEdit(self.formGroupBox_2)

            self.addHn.setObjectName('addH{}'.format(self.ai))
            self.addTn.setObjectName('addT{}'.format(self.ai))

            self.addInfo.removeRow(self.ai + 1)

            self.addInfo.setWidget(self.ai + 1,
                                   QtWidgets.QFormLayout.LabelRole, self.addHn)
            self.addInfo.setWidget(self.ai + 1,
                                   QtWidgets.QFormLayout.FieldRole, self.addTn)

            self.addInfo.setWidget(self.ai + 2,
                                   QtWidgets.QFormLayout.SpanningRole,
                                   self.baddInput)

            self.ai += 1
            self.addHn = None
            self.AddTn = None
        else:
            self.errorh.setText('Проблема!')
            self.errort.setText('Кол-во доп. полей ограничено 4')
            self.errorh.show()
            self.errort.show()

class eEqui(QtWidgets.QGroupBox, frames.Ui_fmEqui):
    def __init__(self, parent, win):
        super(eEqui, self).__init__(parent)
        self.setupUi(self)
        self.groupBox.hide()
        self.show()
        self.errorh.hide()
        self.errort.hide()

        self.eqdata = {'id':None}

        self.bbox.button(QtWidgets.QDialogButtonBox.Save).clicked.connect(
            self.save)
        self.bbox.button(QtWidgets.QDialogButtonBox.Reset).clicked.connect(
            win.freset)
        self.rcomp.toggled.connect(self.sparent)

    def sparent(self, id=0):
        self.lsfind.clear()
        if self.rcomp.isChecked():
            self.groupBox.show()
            self.efind.textChanged.connect(self.find)
            self.parents = db.selectall('equipment', ['name'], "WHERE iscomponent='f'")
            self.parentStr=[]
            row = False
            for i in self.parents:
                if not i[0] in ['id', 'equipment']:
                    self.parentStr.append(i[1])
                if i[0] == id:
                    row = len(self.parentStr) - 1
            self.lsfind.addItems(self.parentStr)
            if row:
                self.lsfind.setCurrentRow(row)
            self.lsfind.currentTextChanged.connect(self.pselect)
        else:
            self.groupBox.hide()
            self.lsfind.clear()

    def find(self, text):
        self.lsfind.clear()
        for i in self.parentStr:
            if i.find(text)!=-1:
                self.lsfind.addItem(i)
    def pselect(self, t):
        self.rcomp.setText(t)

    def save(self):
        e = False
        up = False
        dt = {}

        if self.eqdata['id']:
            up = True

        if self.ename.text():
            dt['name'] = str(self.ename.text())
            self.lname.setStyleSheet('color: #333333;')
        else:
            self.lname.setStyleSheet('color: red;')
            e = True
        if self.ecost.text():
            dt['cost'] = int(self.ecost.text())
            self.lcost.setStyleSheet('color: #333333;')
        else:
            self.lcost.setStyleSheet('color: red;')
            e = True
        if self.rcomp.isChecked():
            dt['iscomponent'] = True
        else: dt['iscomponent'] = False
        if self.rcomp.text():
            st = self.rcomp.text()
            for i in self.parents:
                if i[1]==st:
                    dt['parent_id']=i[0]
                    break
        ifError(self.errorh, True)
        ifError(self.errort, True)
        print(str(dt))
        if e == False:
            if up == True:
                i = 0
                for k,v in dt.items():
                    if not self.eqdata[k] == v:
                        print('{} : {}'.format(k,v))
                        db.update(tb='equipment', col=k,
                            value="'{}'".format(v),
                            param='id={}'.format(self.eqdata['id']))
                        i +=1
                if i>0:
                    self.errorh.setText('Обновлено')
                    ifError(self.errorh, False)
                    self.errort.setText('строк: ' + str(i))
                    ifError(self.errort, False)
            else:
                self.eqdata['id'] = db.addication('equipment', dt)[0][0]
                if self.eqdata['id']:
                    self.errorh.setText('успешно!')
                    self.errort.setText('данные успешно сохранены')
                    ifError(self.errorh, False)
                    ifError(self.errort, False)
                else:
                    ifError(self.errorh, True)
                    ifError(self.errort, True)
                    self.errorh.setText('ошибка!')
                    self.errort.setText('Данные не сохраненны!')
                    self.editor(self.eqdata['id'])
        else:
            self.errorh.setText('ошибка')
            self.errort.setText('заплните пустые поля')
        self.errorh.show()
        self.errort.show()

    def editor(self, id):
        self.eqdata['id'] = id
        self.setTitle('Радактирование оборудования')
        select = db.selectall('equipment', ['name', 'cost', 'parent_id', 'iscomponent'], 'WHERE id={}'.format(self.eqdata['id']))
        for i in range(0, len(select[1])):
            if not select[0][i] in ['equipment', 'id']:
                self.eqdata[select[0][i]] = select[1][i]
        del select

        self.ename.setText(str(self.eqdata['name']))
        self.ecost.setText(str(self.eqdata['cost']))
        if self.eqdata['iscomponent'] == True:
            self.rcomp.setChecked(True)
            if self.eqdata['parent_id']:
                self.sparent(self.eqdata['parent_id'])
        print(str(self.eqdata))

class eOrder(QtWidgets.QGroupBox, frames.Ui_fmOrder):
    def __init__(self, parent, win):
        super(eOrder, self).__init__(parent)
        self.setupUi(self)

        self.bbox.button(QtWidgets.QDialogButtonBox.Save).setText('Сохранить')
        self.bbox.button(QtWidgets.QDialogButtonBox.Reset).setText('Сбросить')

        self.dset.setDateTime(datetime.datetime.now())
        self.dend.setDateTime(datetime.datetime.now())

        self.rset.toggled.connect(lambda: setCurTime(self.rset, self.dset))
        self.rend.toggled.connect(lambda: setCurTime(self.rend, self.dend))

        #Идентификаторы исключений
        #0 - Клиенты; 1 - Техника
        self.exp = [[],[]]
        self.tbeqs.setRowCount(0)
        self.tbeqs.setColumnWidth(1, 120)
        self.tbeqs.setColumnHidden(0, True)

        # для редактирования
        self.orderdata = {
            'id': None,
            'eqs': {}
        }

        self.listStatus = db.selectall('status', ['name'])
        tmlist = iter(self.listStatus)
        next(tmlist)
        for x in tmlist:
            self.cbstatus.addItem(x[1])

        #Названия используются в качестве параментра в self.find()
        self.cbfind.addItems(['Клиент', 'Оборудование', 'Запчасти'])
        self.find(t=self.cbfind.currentText())
        self.cbfind.currentTextChanged.connect(self.find)
        self.bfindadd.clicked.connect(self.fromFinder)
        self.bbox.button(QtWidgets.QDialogButtonBox.Save).clicked.connect(
            self.save)
        self.delRow.clicked.connect(self.dlRow)

        self.tbeqs.itemSelectionChanged.connect(self.slRow)
        self.show()
    def find(self,t):
        print(str(t))
        s=0 #Для выбора исключений
        if t=='Клиент':
            self.listSelect = db.selectall('customers', ['name'])
            self.curSelect = 'customers'
            s = 'cust'
        elif t=='Оборудование':
            self.listSelect = db.selectall('equipment', ['name', 'cost'], 'WHERE iscomponent=FALSE')
            self.curSelect = 'equipment'
            s = 'eq'
        elif t=='Запчасти':
            self.listSelect = db.selectall('equipment', ['name', 'cost'], 'WHERE iscomponent=TRUE')
            self.curSelect = 'equipment'
            s = 'eq'
        else:
            self.efind.setText('Нет информации!')
        self.efind.clear()
        if s == 'cust':
            fFilter(self.listSelect, self.lfind, 'All', self.exp[0])
            self.efind.textChanged.connect(lambda: fFilter(self.listSelect, self.lfind, self.efind.text(), self.exp[0]))
        elif s == 'eq':
            fFilter(self.listSelect, self.lfind, 'All', self.exp[1])
            self.efind.textChanged.connect(lambda: fFilter(self.listSelect, self.lfind, self.efind.text(), self.exp[1]))
    def fromFinder(self):
        try:
            item = self.lfind.currentItem().text()
        except TypeError as te:
            pass
        if item:
            if self.listSelect[0][0]=='customers':
                for x in self.listSelect:
                    if x[1] == item:
                        self.ecust.setText(x[1])
                        self.exp[0] = []
                        self.exp[0].append(x[0])
                        fFilter(self.listSelect, self.lfind, 'All', self.exp[1])
                        self.efind.clear()
            if self.listSelect[0][0]=='equipment':
                for x in self.listSelect:
                    if x[1] == item:
                        self.exp[1].append(x[0])
                        row = self.tbeqs.rowCount()
                        self.tbeqs.setRowCount( row + 1)
                        tm = QtWidgets.QTableWidgetItem(str(x[0]))
                        print(str(x[0]))
                        self.tbeqs.setItem(row, 0, tm)
                        tm = QtWidgets.QTableWidgetItem(str(x[1]))
                        self.tbeqs.setItem(row, 1, tm)
                        tm = QtWidgets.QTableWidgetItem(str(x[2]))
                        self.tbeqs.setItem(row, 2, tm)
                        tm = QtWidgets.QTableWidgetItem(str(1))
                        self.tbeqs.setItem(row, 3, tm)
                        fFilter(self.listSelect, self.lfind, 'All', self.exp[1])
                        self.efind.clear()
        else:
            self.efind.text('Отметьте элемент в списке!')
    def slRow(self):
        self.delRow.setEnabled(True)
        curRow = self.tbeqs.currentRow()
        try:
            self.numEq.setText(str(self.tbeqs.item(curRow, 3).text()))
        except AttributeError as nt:
            pass
        self.numEq.textChanged.connect(self.chNum)
    def chNum(self, t):
        self.tbeqs.item(self.tbeqs.currentRow(), 3).setText(self.numEq.text())

    def dlRow(self):
        el = self.tbeqs.item(self.tbeqs.currentRow(), 0)
        print(str(self.exp))
        print(str(el.text()))
        self.exp[1].remove(int(el.text()))
        self.tbeqs.removeRow(self.tbeqs.currentRow())
        fFilter(self.listSelect, self.lfind, 'All', self.exp[1])
        self.efind.clear()

    def save(self):
        e = False
        up = False
        dt = {}
        dt['eqs'] = {}
        if self.orderdata['id']:
            up = True

        if self.ecust.text():
            self.lcust.setStyleSheet('color: #333333;')
        else:
            self.lcust.setStyleSheet('color: red;')
            e = True

        if self.tbeqs.rowCount() > 0:
            self.leq.setStyleSheet('color: #333333;')
        else:
            self.leq.setStyleSheet('color: red;')
            e = True

        if e == False:
            dt['customer_id'] = self.ecust.text()
            if self.tcomment.toPlainText():
                dt['comment']= self.tcomment.toPlainText()
            else:
                dt['comment'] = ''
            tmdt = self.dset.dateTime()
            dt['date_set'] = tmdt.toString('dd.MM.yyyy HH:MM')
            tmdt = self.dend.date()
            dt['deadline'] = tmdt.toString('dd.MM.yyyy')
            dt['user_id'] = int(usr['id'])
            if self.eadd.text():
                dt['add_cost'] = int(self.eadd.text())
            else:
                dt['add_cost'] = 0
            dt['status_id'] = getId(self.cbstatus.currentText(), self.listStatus)
            dt['customer_id'] = self.exp[0][0]

            for x in range(0, self.tbeqs.rowCount()):
                dt['eqs'][int(self.tbeqs.item(x, 0).text())] = int(self.tbeqs.item(x, 3).text())
            id = 0
            if up == True:
                for k, v in dt.items():
                    if not k in ['orders', 'id', 'eqs']:
                        if self.orderdata[k] != v:
                            db.update('orders', k, '\'{}\''.format(v), 'id={}'.format(self.orderdata['id']))
                            id += 1
                if id>0:
                    ifError(self.errorh, False)
                    self.errorh.setText('Обновлено')
                    ifError(self.errort, False)
                    self.errort.setText('изменений: {}'.format(id))

                if self.orderdata['eqs']:
                    old = list(self.orderdata['eqs'])
                    new = list(dt['eqs'])

                    x = list(set(old) - set(new))
                    for i in x:
                        db.delel('order_to_equipment', i, 'equipment_id', 'and order_id={}'.format(self.orderdata['id']))
                    x = list(set(new) - set(old))
                    for i  in x:
                        tm = {
                            'order_id': self.orderdata['id'],
                            'equipment_id': i,
                            'num': dt['eqs'][i]
                        }
                        db.addication('order_to_equipment', tm, '')
                    x = list(set(old) & set(new))
                    for i in x:
                        if self.orderdata['eqs'][i] != dt['eqs'][i]:
                            db.update('order_to_equipment', 'num', dt['eqs'][i], 'order_id={} AND equipment_id={}'.format(self.orderdata['id'], i))
                self.editor(self.orderdata['id'])
            else:
                eqs = dt['eqs']
                del dt['eqs']
                id = db.addication('orders', dt)[0][0]
                for k,v in eqs.items():
                    tm ={
                        'order_id': id,
                        'equipment_id': k,
                        'num': v
                    }
                    db.addication('order_to_equipment', tm,''  )

                if id:
                    ifError(self.errorh, False)
                    self.errorh.setText('Заказ создан')
                else:
                    ifError(self.errorh, True)
                    self.errorh.setText('Ошибка')
                self.editor(id)
        else:
            ifError(self.errorh,True)
            self.errorh.setText('Ошибка')
            ifError(self.errort, True)
            self.errort.setText('Заполните поля')
        self.errorh.show()
        self.errort.show()
    def editor(self, id):
        self.tbeqs.clear()
        self.tbeqs.setRowCount(0)
        self.bbox.button(QtWidgets.QDialogButtonBox.Save).setText('Изменить')

        dt = {'id': id}
        dataOrder = db.selectall('orders', ['customer_id','comment', 'status_id', 'date_set', 'deadline', 'add_cost', 'user_id'], 'WHERE id={}'.format(id))
        for i in  range(0, len(dataOrder[0])):
            dt[dataOrder[0][i]] = dataOrder[1][i]

        self.orderdata = dt
        del(self.orderdata['orders'])
        self.tcomment.setPlainText(dt['comment'])
        self.cbstatus.setCurrentText(getText(dt['status_id'], self.listStatus, 1))
        self.dset.setDateTime(dt['date_set'])
        self.dend.setDate(dt['deadline'])
        self.eadd.setText(str(dt['add_cost']))

        self.orderdata['date_set'] = dt['date_set'].strftime('%d.%m.%Y %H:%M')
        self.orderdata['deadline'] = dt['deadline'].strftime('%d.%m.%Y')

        self.exp[0].append(dt['customer_id'])
        cust = db.selectall('customers', ['name'], 'WHERE id={}'.format(dt['customer_id']))[1][1]
        self.ecust.setText(cust)

        linkOrder = iter(db.selectall('order_to_equipment', ['equipment_id', 'num'], 'WHERE order_id = {}'.format(id), with_id=False))
        next(linkOrder)
        linkOrder = list(linkOrder)
        if len(linkOrder) > 0:
            tms = ''
            self.orderdata['eqs'] = {}
            for x in linkOrder:
                tms += '{} ,'.format(x[0])
                self.exp[1].append(x[0])
                self.orderdata['eqs'][x[0]]= x[1]
            tms = tms[:-2]
            print(str(self.orderdata['eqs']))
            ordereqs = iter(db.selectall('equipment', ['name', 'cost'],
                                    'WHERE id in({})'.format(tms)))
            next(ordereqs)
            for x in ordereqs:
                self.exp[1].append(x[0])
                row = self.tbeqs.rowCount()
                self.tbeqs.setRowCount(row + 1)
                tm = QtWidgets.QTableWidgetItem(str(x[0]))
                print(str(x[0]))
                self.tbeqs.setItem(row, 0, tm)
                tm = QtWidgets.QTableWidgetItem(str(x[1]))
                self.tbeqs.setItem(row, 1, tm)
                tm = QtWidgets.QTableWidgetItem(str(x[2]))
                self.tbeqs.setItem(row, 2, tm)
                tm = QtWidgets.QTableWidgetItem(str(self.orderdata['eqs'][x[0]]))
                self.tbeqs.setItem(row, 3, tm)
                fFilter(self.listSelect, self.lfind, 'All', self.exp[1])
                self.efind.clear()

class fView(QtWidgets.QGroupBox, frames.Ui_fmView):
    def __init__(self, parent, win):
        super(fView, self).__init__(parent)
        self.setupUi(self)
        self.p = win

        self.tb.setHorizontalHeader
        self.cur_id = False
        self.show()


    def start(self, tb):
        self.table=tb
        if tb=='orders':
            self.header = ['Номер', 'Заказчик', 'Дата подачи', 'Дата сдачи', 'Стоимость оборуд.', 'Наценка', 'Статус']
            select = db.selectorder()
            siter = iter(select)
            next(siter)
            self.select = list(siter)
            self.model = tbModel(self.select, self.header, self)
            self.tb.setModel(self.model)
            self.tb.hideColumn(0)
            self.bedit.clicked.connect(lambda: self.p.editorder(self.cur_id))

        if tb=='customers':
            self.header = ['Номер', 'Название', 'Директор', 'Номер', 'E-mail']
            select = fromSecond(db.selectall('customers', ['name', 'director', 'contact_phone','contact_email']))
            self.model = tbModel(select, self.header, self)
            self.tb.setModel(self.model)
            self.tb.hideColumn(0)
            self.bedit.clicked.connect(lambda: self.p.editcust(self.cur_id))

        if tb=='equipment':
            self.header = ['Номер', 'Название', 'Стоимость', 'Запчасть']
            select = iter(db.selectall('equipment', ['name', 'cost', 'parent_id', 'iscomponent']))
            next(select)
            select = list(select)
            self.select = []
            for i in select:
                tm = ''
                par =db.selectall('equipment', ['name'], "WHERE iscomponent='f'")
                if i[4]:
                    if i[3]:
                        tm=getText(i[3], par)
                    else:
                        tm = 'Да'
                else:
                    tm = 'Нет'
                self.select.append([i[0], i[1], i[2], tm])
            print(str(len(self.select)))
            self.model = tbModel(self.select, self.header, self)
            self.tb.setModel(self.model)
            self.tb.hideColumn(0)
            self.bedit.clicked.connect(lambda: self.p.editequip(self.cur_id))


        self.tb.setSortingEnabled(True)
        self.slModel = self.tb.selectionModel()
        self.slModel.selectionChanged.connect(self.slRow)
        self.bdel.clicked.connect(self.delRow)

    def slRow(self, i):
        if (len(i.indexes())):
            ind = i.indexes()[0]
            self.cur_id = self.model.data(ind, Qt.DisplayRole)
            self.bedit.setEnabled(True)
            self.bdel.setEnabled(True)
            self.lbtn.setText('Строка выделена')
        else:
            self.bedit.setEnabled(False)
            self.bdel.setEnabled(False)
            self.lbtn.setText('Выделите строку')
    def delRow(self):
        if self.cur_id:
            if self.table == 'orders':
                db.delel('order_to_equipment', self.cur_id, 'order_id')
            if self.table == 'equipment':
                parent = True
                parent = db.selectall('equipment','iscomponent', 'WERE id={}'.format(self.cur_id))
                if parent == False:
                    print(str(parent))
                    db.delel('equipment', self.cur_id, 'parent_id')
                db.delel('order_to_equipment', self.cur_id, 'equipment_id')
            db.delel(self.table, '{}'.format(self.cur_id))
            self.start(self.table)
